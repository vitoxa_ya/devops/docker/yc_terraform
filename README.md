# Dockerfile Terraform
---
CI для создания Docker образа Terraform c провайдерами Яндекс.Cloud/VK.Cloud
-------------
---
Docker Build Arg
--------------
      # Прокси для скачивания бинарника с сайт hashicorp.com
      PROXY="35.185.196.38:3128"
      # Версия terraform файла
      TF_VERSION="1.8.1"
      # Архитектура бинарника
      PLATFORM="amd64"
---      
Example Build command
----------------
      docker build -t tf:scratch --build-arg PROXY="35.185.196.38:3128" --build-arg TF_VERSION="1.8.1" .
---
License
-------

MIT

Author Information
------------------

Подрезенко Виталий
@vitoxaya
