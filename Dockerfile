FROM alpine:3.19 as build

ENV LANG C.UTF-8

ARG PROXY
ARG TF_VERSION
ARG PLATFORM="amd64"

ENV ROOT_BIN="bin"
ENV ROOT_LIB="lib"
ENV ROOT_ETC="etc"

ENV USR_BIN="usr/bin"
ENV USR_SBIN="usr/sbin"
ENV USR_SHARE="usr/share"

ENV TMP_DIR="tmp/root"

RUN apk add --update-cache --no-cache unzip git wget ca-certificates
# 
# crate need directory
RUN mkdir -p /${TMP_DIR}/${ROOT_BIN} /${TMP_DIR}/${ROOT_LIB} /${TMP_DIR}/${USR_BIN} /${TMP_DIR}/app /${TMP_DIR}/tmp

# get terraform 
RUN https_proxy="${PROXY}" wget https://releases.hashicorp.com/terraform/$TF_VERSION/terraform_${TF_VERSION}_linux_${PLATFORM}.zip && \
    unzip terraform_*.zip && \
    rm terraform_*.zip && \
    mv terraform /${TMP_DIR}/${USR_BIN}/
# 
# terraform cloud config
COPY ./.terraformrc /${TMP_DIR}/app
#
# sheel
RUN cp -a /${ROOT_BIN}/busybox /${ROOT_BIN}/sh /${ROOT_BIN}/echo /${ROOT_BIN}/mkdir /${ROOT_BIN}/chmod /${ROOT_BIN}/grep /${ROOT_BIN}/touch /${TMP_DIR}/${ROOT_BIN}/
RUN cp -a /${USR_BIN}/env /${TMP_DIR}/${USR_BIN}/
RUN cp -a /${ROOT_LIB}/ld-musl-* /${ROOT_LIB}/libc.musl-* /${TMP_DIR}/${ROOT_LIB}/
#
# ca-sertificates
RUN mkdir -p /${TMP_DIR}/${ROOT_ETC}/ssl/ &&\
    cp -a /${ROOT_ETC}/ssl/certs/ /${TMP_DIR}/${ROOT_ETC}/ssl/
RUN mkdir -p /${ROOT_LIB}/${USR_SBIN} /${USR_SHARE}/ca-certificates &&\
    cp -a /${USR_SBIN}/update-ca-certificates /${TMP_DIR}/${USR_SBIN} &&\
    cp -ar /${ROOT_ETC}/ca-certificates* /${TMP_DIR}/${ROOT_ETC}/ &&\
    cp -ar /${USR_SHARE}/ca-certificates* /${TMP_DIR}/${USR_SHARE}/

FROM scratch

ENV HOME /app
ENV LANG C.UTF-8
ENV PATH /usr/bin:/bin

WORKDIR ${HOME}

COPY --from=build /tmp/* /

LABEL "License"="MIT" "author"="Подрезенко Виталий @vitoxaya"

ENTRYPOINT ["terraform", "--version"]
